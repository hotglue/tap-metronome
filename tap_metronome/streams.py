"""Stream type classes for tap-metronome."""

from typing import Any, Dict, Iterable, Optional

import requests
from singer_sdk import typing as th
from singer_sdk.helpers.jsonpath import extract_jsonpath

from tap_metronome.client import MetronomeStream


class CustomersStream(MetronomeStream):
    """Define custom stream."""

    name = "customers"
    path = "/customers"
    primary_keys = ["id"]
    replication_key = None

    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("external_id", th.StringType),
        th.Property("name", th.StringType),
        th.Property(
            "customer_config",
            th.ObjectType(th.Property("salesforce_account_id", th.StringType)),
        ),
    ).to_dict()

    def get_records(self, context: Optional[dict]) -> Iterable[Dict[str, Any]]:
        if self.config.get("customers"):
            records = [{"id": c} for c in self.config["customers"].split(",")]
        else:
            records = self.request_records(context)
        for record in records:
            transformed_record = self.post_process(record, context)
            if transformed_record is None:
                continue
            yield transformed_record

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return {
            "customer_id": record["id"],
            "billing_provider_type": self.config.get("billing_provider_type"),
        }


class BillableMetricsStream(MetronomeStream):
    """Define custom stream."""

    name = "billable_metrics"
    path = "/customers/{customer_id}/billable-metrics"
    primary_keys = ["id"]
    replication_key = None
    parent_stream_type = CustomersStream

    schema = th.PropertiesList(
        th.Property("customer_id", th.StringType),
        th.Property("id", th.StringType),
        th.Property("name", th.StringType),
        th.Property("group_by", th.CustomType({"type": ["array", "string"]})),
    ).to_dict()


class BillingConfigurationStream(MetronomeStream):
    """Define custom stream."""

    name = "billing_configuration"
    path = "/customers/{customer_id}/billing-config/{billing_provider_type}"
    primary_keys = ["billing_provider_customer_id"]
    replication_key = None
    parent_stream_type = CustomersStream

    schema = th.PropertiesList(
        th.Property("customer_id", th.StringType),
        th.Property("billing_provider_type", th.StringType),
        th.Property("billing_provider_customer_id", th.StringType),
        th.Property("stripe_collection_method", th.StringType),
        th.Property("aws_product_code", th.StringType),
        th.Property("aws_region", th.StringType),
        th.Property("aws_expiration_date", th.DateTimeType),
    ).to_dict()


class UsageStream(MetronomeStream):
    """Define custom stream."""

    name = "usage"
    path = "/usage"
    primary_keys = [
        "customer_id",
        "billable_metric_id",
        "billable_metric_name",
        "start_timestamp",
    ]
    replication_key = "end_timestamp"
    rest_method = "POST"
    schema = th.PropertiesList(
        th.Property("customer_id", th.StringType),
        th.Property("billable_metric_id", th.StringType),
        th.Property("billable_metric_name", th.StringType),
        th.Property("start_timestamp", th.DateTimeType),
        th.Property("end_timestamp", th.DateTimeType),
        th.Property("value", th.NumberType),
    ).to_dict()

    def prepare_request_payload(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Optional[dict]:
        payload = {}
        dates = self.get_start_enddates(context)
        payload["starting_on"] = dates["start_date"]
        payload["ending_before"] = dates["end_date"]
        payload["window_size"] = self.config.get("window_size")
        if self.config.get("customers"):
            payload["customer_ids"] = [c for c in self.config["customers"].split(",")]
        return payload

    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        """Parse the response and return an iterator of result rows."""
        for row in extract_jsonpath(self.records_jsonpath, input=response.json()):
            if row.get("value"):
                yield row


class CostsStream(MetronomeStream):
    """Define custom stream."""

    name = "costs"
    path = "/customers/{customer_id}/costs"
    primary_keys = ["customer_id", "start_timestamp"]
    replication_key = "end_timestamp"
    parent_stream_type = CustomersStream

    schema = th.PropertiesList(
        th.Property("customer_id", th.StringType),
        th.Property("start_timestamp", th.DateTimeType),
        th.Property("end_timestamp", th.DateTimeType),
        th.Property("credit_types", th.CustomType({"type": ["object", "string"]})),
    ).to_dict()


class PlansStream(MetronomeStream):
    """Define custom stream."""

    name = "plans"
    path = "/customers/{customer_id}/plans"
    primary_keys = None
    replication_key = None
    parent_stream_type = CustomersStream

    schema = th.PropertiesList(
        th.Property("customer_id", th.StringType),
        th.Property("id", th.StringType),
        th.Property("plan_id", th.StringType),
        th.Property("starting_on", th.DateTimeType),
        th.Property("trial_info", th.CustomType({"type": ["object", "string"]})),
    ).to_dict()


class InvoicesStream(MetronomeStream):
    """Define custom stream."""

    name = "invoices"
    path = "/customers/{customer_id}/invoices"
    primary_keys = ["id"]
    replication_key = None
    parent_stream_type = CustomersStream

    schema = th.PropertiesList(
        th.Property("customer_id", th.StringType),
        th.Property("id", th.StringType),
        th.Property("start_timestamp", th.DateTimeType),
        th.Property("end_timestamp", th.DateTimeType),
        th.Property(
            "credit_type",
            th.ObjectType(
                th.Property("id", th.StringType),
                th.Property("name", th.StringType),
            ),
        ),
        th.Property(
            "line_items",
            th.ArrayType(
                th.ObjectType(
                    th.Property(
                        "credit_type",
                        th.ObjectType(
                            th.Property("id", th.StringType),
                            th.Property("name", th.StringType),
                        ),
                    ),
                    th.Property("name", th.StringType),
                    th.Property("quantity", th.NumberType),
                    th.Property("total", th.NumberType),
                    th.Property(
                        "sub_line_items", th.CustomType({"type": ["array", "string"]})
                    ),
                )
            ),
        ),
        th.Property(
            "invoice_adjustments", th.CustomType({"type": ["array", "string"]})
        ),
        th.Property("status", th.StringType),
        th.Property("subtotal", th.NumberType),
        th.Property("total", th.NumberType),
    ).to_dict()

    def post_process(self, row: dict, context: Optional[dict] = None) -> Optional[dict]:
        row.update({"customer_id": context.get("customer_id")})
        return row
