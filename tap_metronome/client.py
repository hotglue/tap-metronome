"""REST client handling, including MetronomeStream base class."""


import copy
from datetime import datetime, timedelta
from typing import Any, Dict, Iterable, Optional

import requests
from singer_sdk.authenticators import BearerTokenAuthenticator
from singer_sdk.exceptions import FatalAPIError, RetriableAPIError
from singer_sdk.helpers.jsonpath import extract_jsonpath
from singer_sdk.streams import RESTStream


class MetronomeStream(RESTStream):
    """Metronome stream class."""

    _page_size = 100
    url_base = "https://api.metronome.com/v1"

    records_jsonpath = "$.data[*]"
    next_page_token_jsonpath = "$.next_page"

    @property
    def authenticator(self) -> BearerTokenAuthenticator:
        """Return a new authenticator object."""
        return BearerTokenAuthenticator.create_for_stream(
            self, token=self.config.get("auth_token")
        )

    @property
    def http_headers(self) -> dict:
        """Return the http headers needed."""
        headers = {}
        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent")
        return headers

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""
        if self.next_page_token_jsonpath:
            all_matches = extract_jsonpath(
                self.next_page_token_jsonpath, response.json()
            )
            first_match = next(iter(all_matches), None)
            next_page_token = first_match

        return next_page_token

    def get_start_enddates(self, context):
        dt_format = "%Y-%m-%dT00:00:00.000Z"
        start_date = self.get_starting_timestamp(context)
        if not start_date:
            yesterday = (datetime.today() - timedelta(days=1)).strftime(dt_format)
            start_date = self.config.get("start_date", yesterday)
        else:
            start_date = start_date.replace(tzinfo=None)
            min_date = datetime(2020, 4, 13)
            if start_date < min_date:
                start_date = min_date
            start_date = start_date.strftime(dt_format)
        end_date = self.config.get("end_date", datetime.today().strftime(dt_format))
        return {"start_date": start_date, "end_date": end_date}

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        params: dict = {}
        dates = self.get_start_enddates(context)
        start_date = dates["start_date"]
        end_date = dates["end_date"]
        if self.replication_key:
            params["starting_on"] = start_date
            params["ending_before"] = end_date
        params["limit"] = self._page_size
        if next_page_token:
            params["next_page"] = next_page_token

        return params

    def post_process(self, row: dict, context: Optional[dict]) -> dict:
        """As needed, append or transform raw data to match expected structure."""
        if context:
            row.update(context)
        return row

    def request_records(self, context: Optional[dict]) -> Iterable[dict]:
        """Request records from REST endpoint(s), returning response records."""
        next_page_token: Any = None
        finished = False
        decorated_request = self.request_decorator(self._request)

        while not finished:
            valid_request = True
            if self.replication_key and self.rest_method == "GET":
                params = self.get_url_params(context, next_page_token)
                if params["starting_on"] == params["ending_before"]:
                    valid_request = False

            if valid_request:
                prepared_request = self.prepare_request(
                    context, next_page_token=next_page_token
                )
                resp = decorated_request(prepared_request, context)
                yield from self.parse_response(resp)
                previous_token = copy.deepcopy(next_page_token)
                next_page_token = self.get_next_page_token(
                    response=resp, previous_token=previous_token
                )
                if next_page_token and next_page_token == previous_token:
                    raise RuntimeError(
                        f"Loop detected in pagination. "
                        f"Pagination token {next_page_token} is identical to prior token."
                    )
            # Cycle until get_next_page_token() no longer returns a value
            finished = not next_page_token

    def validate_response(self, response: requests.Response) -> None:
        if response.status_code == 500 and self.name in [
            "plans",
            "costs",
            "billing_configuration",
            "billable_metrics",
            "usage",
        ]:
            msg = self.response_error_message(response)
        elif (
            response.status_code in self.extra_retry_statuses
            or 500 <= response.status_code < 600
        ):
            msg = self.response_error_message(response)
            raise RetriableAPIError(msg, response)
        elif response.status_code == 400 and self.replication_key:
            self.logger.warning(response.json().get("message"))
        elif 400 <= response.status_code < 500 and self.name != "billing_configuration":
            msg = self.response_error_message(response)
            raise FatalAPIError(msg)
