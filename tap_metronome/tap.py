"""Metronome tap class."""

from email.policy import default
from typing import List

from singer_sdk import Stream, Tap
from singer_sdk import typing as th

from tap_metronome.streams import (
    BillableMetricsStream,
    BillingConfigurationStream,
    CostsStream,
    CustomersStream,
    InvoicesStream,
    PlansStream,
    UsageStream,
)

STREAM_TYPES = [
    CustomersStream,
    BillingConfigurationStream,
    BillableMetricsStream,
    UsageStream,
    CostsStream,
    PlansStream,
    InvoicesStream,
]


class TapMetronome(Tap):
    """Metronome tap class."""

    name = "tap-metronome"

    config_jsonschema = th.PropertiesList(
        th.Property(
            "auth_token",
            th.StringType,
            required=True,
            description="The token to authenticate against the API service",
        ),
        th.Property("start_date", th.DateTimeType),
        th.Property("end_date", th.DateTimeType),
        th.Property("billing_provider_type", th.StringType, default="stripe"),
        th.Property("window_size", th.StringType, default="day"),
        th.Property("customers", th.StringType),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == "__main__":
    TapMetronome.cli()
